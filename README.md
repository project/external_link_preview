About External Link Preview for Drupal
-------------------------
This module allows users to preview all the external links
that are added into any text area field by fetching their
metatags - title, description and image.

Help & Documentation
--------------------
In case you have added any external link in any textarea
and want to preview the external ink,
please follow the following steps :

1. Go to formatter settings of the particluar textarea field
   under "Manage Display" section of the Entity Type.
2. Select the formatter "Attach Link Preview" from the format select list.
3. Click on the setting of the formatter and choose your desired options.

Note - You can preview the external link iff the field has format of type "Long text" or "Long text with summary" like Body field of any content type.
