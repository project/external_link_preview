<?php
/**
 * @file
 * Theme implementation to preview external link fetched from metatags.
 *
 * - $title: The title of the external link.
 * - $description: the body of the external link.
 * - $image: the image fetched from external link.
 * - $url: the URL of the external link.
 *
 * @ingroup themable
 */
?>
<div class="outerlink-wrapper">
  <?php if ($image): ?>
    <div class="outerlink-image"><?php print $image; ?></div>
  <?php endif; ?>
  <?php if ($title): ?>
    <div class="outerlink-title"><a href="<?php print $url;?>" target="_blank"><?php print $title;?></a></div>
  <?php endif; ?>
  <?php if ($description): ?>
    <div class="outerlink-description"><?php print $description; ?></div>
  <?php endif; ?>
</div>
